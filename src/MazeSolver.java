/*  Travis Dattilo
    cssc0886
*/

import data_structures.*;

public class MazeSolver {
	private int dimensionOf;
	private MazeGrid mazeGridGUI;
	
	private Queue<GridCell> markQueue;
	private Stack<GridCell> moveStack;

	public MazeSolver(int dimension){	
		dimensionOf = dimension;

		markQueue = new Queue<GridCell>();
		moveStack = new Stack<GridCell>();
		mazeGridGUI = new MazeGrid(this, dimension);
	}
	
	public void mark(){ 					//uses Queue
		GridCell start = mazeGridGUI.getCell(0,0);	//beginning of the maze
		start.setDistance(0);
		mazeGridGUI.markDistance(start);
		markQueue.enqueue(start);			//enqueue (0,0)
				
		while(!markQueue.isEmpty()){
			GridCell current = markQueue.dequeue();	//dequeue a GridCell from the queue
			int currentDistance = current.getDistance();
			int currentX = current.getX();
			int currentY = current.getY();
			GridCell up, down, left, right;
			
			//up (0,-1) down(0,1) left(-1,0) right(1,0)
			up = mazeGridGUI.getCell(currentX + 0, currentY - 1);
			down = mazeGridGUI.getCell(currentX + 0, currentY + 1);
			left = mazeGridGUI.getCell(currentX + -1, currentY + 0);
			right = mazeGridGUI.getCell(currentX + 1, currentY + 0);
			
			//Checks if adjacent cells to current are valid and have not been visited
			//and marks and enqueues if those conditions are met.
			if(mazeGridGUI.isValidMove(up) && !up.wasVisited()){
				up.setDistance(currentDistance + 1);
				mazeGridGUI.markDistance(up);
				markQueue.enqueue(up);
			}
			
			if(mazeGridGUI.isValidMove(down) && !down.wasVisited()){
				down.setDistance(currentDistance + 1);
				mazeGridGUI.markDistance(down);
				markQueue.enqueue(down);
			}
			
			if(mazeGridGUI.isValidMove(left) && !left.wasVisited()){
				left.setDistance(currentDistance + 1);
				mazeGridGUI.markDistance(left);
				markQueue.enqueue(left);
			}
			
			if(mazeGridGUI.isValidMove(right) && !right.wasVisited()){
				right.setDistance(currentDistance + 1);
				mazeGridGUI.markDistance(right);
				markQueue.enqueue(right);
			}
								
		}
		
	}
	public boolean move(){ 				//uses Stack
		GridCell terminalCell = mazeGridGUI.getCell(dimensionOf - 1, dimensionOf - 1);
		int distance = terminalCell.getDistance();	//distance from origin of the terminal cell of the maze
		
		if(distance == -1) return false; 	//unreachable, puzzle has no solution
		moveStack.push(terminalCell);		//push terminal cell onto the stack
				
		while(distance != 0){		
			GridCell stackPeek = moveStack.peek();	
			int stackPeekX = stackPeek.getX();
			int stackPeekY = stackPeek.getY();
			GridCell up, down, left, right;
			GridCell tmp = stackPeek;
			
			//up (0,-1) down(0,1) left(-1,0) right(1,0)	
			up = mazeGridGUI.getCell(stackPeekX + 0, stackPeekY - 1);
			down = mazeGridGUI.getCell(stackPeekX + 0, stackPeekY + 1);
			left = mazeGridGUI.getCell(stackPeekX + -1, stackPeekY + 0);
			right = mazeGridGUI.getCell(stackPeekX + 1, stackPeekY + 0);
			
			//Checks the adjacent cases for the smallest path from terminal cell to initial cell.
			//Gets the distance from each cell adjacent to the top of the stack.
			//Select the cell with the smallest distance and pushes it onto the stack.
			if(mazeGridGUI.isValidMove(up) && up.getDistance() < tmp.getDistance())
				tmp = up;
			
			if(mazeGridGUI.isValidMove(down) && down.getDistance() < tmp.getDistance())
				tmp = down;

			if(mazeGridGUI.isValidMove(left) && left.getDistance() < tmp.getDistance())
				tmp = left;	

			if(mazeGridGUI.isValidMove(right) && right.getDistance() < tmp.getDistance())						
				tmp = right;
			
			moveStack.push(tmp);
			distance--;
		}
		
		while(!moveStack.isEmpty()){
			mazeGridGUI.markMove(moveStack.pop());	
		}				
		return true;
	}
	
	public void reset(){
		markQueue.makeEmpty();
		moveStack.makeEmpty(); 
	}
	
	public static void main(String [] args) {
		new MazeSolver(new Integer(50));
	}
}
