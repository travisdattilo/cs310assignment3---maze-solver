## Project Overview

***NOTE: GridCell.java and MazeGrid.java were supplied by the professor***

- **GridCell.java** is wrapper class that holds xy coordinates and distance from origin (*created by professor*)
- **MazeGrid.java** is the GUI for the MazeSolver class (*created by professor*)

Using my Stack and Queue implementations using the LinearList class (from previous projects), create a maze solver that can navigate through the mazes created MazeGrid.java

- **LinearListADT.java** is the linear list interface
- **LinearList.java** is the implementation of the *LinearListADT* interface
- **Stack.java** is the stack implementation that uses *LinearList*
- **Queue.java** is the queue implementation that uses *LinearList*
- **MazeSolver.java** solves the maze created by *MazeGrid* using *Queue* and *Stack*

## LinearList Functions

- **addFirst:** Adds the Object obj to the beginning of list and returns true if the list is not full. Returns false and aborts the insertion if the list is full.
- **addLast:** Adds the Object obj to the end of list and returns true if the list is not full. Returns false and aborts the insertion if the list is full.
- **removeFirst:** Removes and returns the parameter object obj in first position in list if the list is not empty, null if the list is empty.
- **removeLast:** Removes and returns the parameter object obj in last position in list if the list is not empty, null if the list is empty.
- **remove:** Removes and returns the parameter object obj from the list if the list contains it, null otherwise. The ordering of the list is preserved. The list may contain duplicate elements. This method removes and returns the first matching element found when traversing the list from first position. Shift elements to fill in the slot where the deleted element was located.
- **peekFirst:** Returns the first element in the list, null if the list is empty. The list is not modified.
- **peekLast:** Returns the last element in the list, null if the list is empty. The list is not modified.
- **contains:** Returns true if the parameter object obj is in the list, false otherwise. The list is not modified.
- **find:** Returns the element matching obj if it is in the list, null otherwise. In the case of duplicates, this method returns the element closest to front. The list is not modified.
- **clear:** The list is returned to an empty state.
- **isEmpty:** Returns true if the list is empty, otherwise false
- **isFull:** Returns true if the list is full, otherwise false
- **size:** Returns the number of Objects currently in the list.
- **iterator:** Returns an Iterator of the values in the list, presented in the same order as the underlying order of the list. (front first, rear last)

## Queue Functions

- **enqueue:** inserts the object obj into the queue
- **dequeue:** removes and returns the object at the front of the queue
- **size:** returns the number of objects currently in the queue
- **isEmpty:** returns true if the queue is empty, otherwise false
- **peek:** returns but does not remove the object at the front of the queue
- **contains:** returns true if the Object obj is in the queue
- **makeEmpty:** returns the queue to an empty state
- **remove:** removes the Object obj if it is in the queue and returns true, otherwise returns false.
- **Iterator:** returns an iterator of the elements in the queue. The elements must be in the same sequence as dequeue would return them.

## Stack Functions

- **push:** inserts the object obj into the stack
- **pop:** pops and returns the element on the top of the stack 
- **size:** returns the number of elements currently in the stack
- **isEmpty:** return true if the stack is empty, otherwise false
- **peek:** returns but does not remove the element on the top of the stack 
- **contains:** returns true if the object obj is in the stack, otherwise false  
- **makeEmpty:** returns the stack to an empty state
- **remove:** removes the Object obj if it is in the stack and returns true, otherwise returns false.
- **Iterator:** returns a iterator of the elements in the stack.  The elements must be in the same sequence as pop() would return them.  

## MazeSolver Functions

- **mark:** uses the queue to mark where (what gridcell) the program has already visited and checks all adjacent edge cases for valid moves
- **move:** uses the stack to move through the maze and checks all adjacent edge cases for shortest path distances
- **reset:** empties the queue and stack to reset the maze search
